
public class HolaMundo {
	public static void main(String[] args) {
		Calculadora c =  new Calculadora();
		c.registar("+", new Suma());
		c.registar("-", new Resta());
		int x = c.ejecutar(3, 5, "-");
		System.out.println("suma 3-5=" + x);
	}
}
